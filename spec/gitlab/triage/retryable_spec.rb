require 'spec_helper'

require 'gitlab/triage/retryable'

describe Gitlab::Triage::Retryable do
  before do
    stub_const('RetryError', Class.new(StandardError))
    stub_const('UnexpectedError', Class.new(StandardError))
    stub_const('Retryer', Class.new.include(described_class))
  end

  subject { Retryer.new }

  describe '#execute_with_retry' do
    context 'when an exception is raised' do
      describe 'default behavior' do
        it 'retries StandardError 3 times' do
          begin
            subject.execute_with_retry { raise RetryError }
          rescue RetryError
            expect(subject.tries).to eq(3)
          end
        end
      end

      it 'retries the given exception MAX_RETRIES times' do
        begin
          subject.execute_with_retry(RetryError) { raise RetryError }
        rescue RetryError
          expect(subject.tries).to eq(described_class::MAX_RETRIES)
        end
      end

      it 'raises the final exception' do
        expect do
          subject.execute_with_retry(RetryError) { raise RetryError }
        end.to raise_error(RetryError)
      end

      it 'ignores unexpected exception' do
        expect do
          subject.execute_with_retry(RetryError) { raise UnexpectedError }
        end.to raise_error(UnexpectedError)
        expect(subject.tries).to eq(1)
      end
    end

    context 'when no exception is raised' do
      it 'returns the result' do
        result = subject.execute_with_retry { 5 }

        expect(result).to eq(5)
      end
    end
  end
end
